package com.putevodka.myweatherapp

import android.app.Activity
import android.app.Application
import com.bumptech.glide.annotation.GlideModule
import com.putevodka.myweatherapp.di.component.DaggerAppComponent
import com.putevodka.myweatherapp.di.modules.AppModule
import com.putevodka.myweatherapp.di.modules.NetModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


//@Suppress("DEPRECATION")
class WeatherApplication: Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    companion object {
        val API_KEY = "5be5dbd5f89dde31441709098116c545"
        var longitude: Double? = 37.6173
        var latitude: Double? = 55.7558
    }

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .netModule(NetModule(BuildConfig.URL))
            //.netModule(NetModule("https://api.openweathermap.org/data/2.5/"))
            .build().inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
}