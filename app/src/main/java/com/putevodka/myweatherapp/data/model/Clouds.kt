package com.putevodka.myweatherapp.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Clouds(
    @Json(name = "all")
    val all : Int
):Serializable
