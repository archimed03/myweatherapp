package com.putevodka.myweatherapp.data.model

import com.squareup.moshi.Json
import java.io.Serializable


data class Coord(
    @Json(name = "lon")
    val lon : Double = 0.0,

    @Json(name = "lat")
    val lat : Double = 0.0
):Serializable
