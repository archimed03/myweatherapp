package com.putevodka.myweatherapp.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Main(
    @Json(name = "temp")
    val temp : Double,

    @Json(name = "feels_like")
    val feels_like : Double,

    @Json(name = "temp_min")
    val temp_min : Double,

    @Json(name = "temp_max")
    val temp_max : Double,

    @Json(name = "pressure")
    val pressure : Int,

    @Json(name = "humidity") val humidity : Int
):Serializable
