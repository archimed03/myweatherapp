package com.putevodka.myweatherapp.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Sys(
    @Json(name = "type")
    val type : Int,

    @Json(name = "id")
    val id : Int,

    //@Json(name = "message")
    //val message : Double,

    @Json(name = "country")
    val country : String,

    @Json(name = "sunrise")
    val sunrise : Int,

    @Json(name = "sunset")
    val sunset : Int
):Serializable
