package com.putevodka.myweatherapp.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Weather(
    @Json(name = "id")
    val id : Int,

    @Json(name = "main")
    val main : String,

    @Json(name = "description")
    val description : String,

    @Json(name = "icon")
    val icon : String
):Serializable
