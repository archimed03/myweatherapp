package com.putevodka.myweatherapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity (
    tableName = "weathertable"
)

data class WeatherBase(

    //@PrimaryKey(autoGenerate = true)
    //val id_internal: Int,

    @Json(name = "coord")
    val coord : Coord,

    @Json(name = "weather")
    val weather : List<Weather>,

    @Json(name = "base")
    val base : String,

    @Json(name = "main")
    val main : Main,

    @Json(name = "visibility")
    val visibility : Int,

    @Json(name = "wind")
    val wind : Wind,

    @Json(name = "clouds")
    val clouds : Clouds,

    @PrimaryKey(autoGenerate = true)
    @Json(name = "dt")
    val dt : Int,

    @Json(name = "sys")
    val sys : Sys,

    @Json(name = "timezone")
    val timezone : Int,

    @Json(name = "id")
    val id_city : Int,

    @Json(name = "name")
    val name : String,

    @Json(name = "cod")
    val cod : Int
)
