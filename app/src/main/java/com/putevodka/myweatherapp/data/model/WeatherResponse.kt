package com.putevodka.myweatherapp.data.model

data class WeatherResponse(
    val weather: WeatherBase
)
