package com.putevodka.myweatherapp.data.model

import androidx.room.TypeConverter
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson


class WeatherTypeAdapter {

    @TypeConverter
    @ToJson
    fun coordToString(value: Coord): String {
        return value.lat.toString() + ","+ value.lon.toString()
    }
    @TypeConverter
    @FromJson
    fun stringToCoord(str: String): Coord {
        val delimiter = ","
        val parts = str.split(delimiter)
        val lon = parts[0].toDouble()
        val lat = parts[1].toDouble()
        val coordinates = Coord(lon, lat)
        return coordinates
    }

    @TypeConverter
    @ToJson
    fun listToString(list: List<Weather>): String {
        var lst = ArrayList<String>()
        for(ls in list) {
            var str = ls.id.toString() + "," + ls.main + "," + ls.description + "," + ls.icon
            lst.add(str)
        }
        val lst_fin = lst.toList()
        return lst_fin.joinToString(";")
    }
    @TypeConverter
    @FromJson
    fun stringToList(str: String): List<Weather> {
        var buffer = ArrayList<Weather>()
        val delimiter = ","
        val parts = str.split(";")
        for (part in parts) {
            val fields = part.split(delimiter)
            //for(field in fields){
                val id = fields[0].toInt()
                val main = fields[1].toString()
                val description = fields[2].toString()
                val icon = fields[3].toString()
                val weather = Weather(id, main, description, icon)
                buffer.add(weather)
            //}
        }
    return buffer.toList()
    }

    @TypeConverter
    @ToJson
    fun sysToString(value: Sys): String {
        return value.type.toString() + ","+ value.id.toString() +
                "," + value.country + "," + value.sunrise.toString() + "," + value.sunset.toString()
    }
    @TypeConverter
    @FromJson
    fun stringToSys(str: String): Sys {
        val delimiter = ","
        val parts = str.split(delimiter)
        val type = parts[0].toInt()
        val id = parts[1].toInt()
        //val message = parts[2].toDouble()
        val country = parts[2]
        val sunrise = parts[3].toInt()
        val sunset = parts[4].toInt()
        //val sys = Sys(type, id, message, country, sunrise, sunset)
        val sys = Sys(type, id, country, sunrise, sunset)
        return sys
    }

    @TypeConverter
    @ToJson
    fun mainToString(value: Main): String {
        return value.temp.toString() + ","+ value.feels_like.toString() + "," + value.temp_min.toString() +
                "," + value.temp_max.toString() + "," + value.pressure.toString() + "," + value.humidity.toString()
    }
    @TypeConverter
    @FromJson
    fun stringToMain(str: String): Main {
        val delimiter = ","
        val parts = str.split(delimiter)
        val temp = parts[0].toDouble()
        val feels_like = parts[1].toDouble()
        val temp_min = parts[2].toDouble()
        val temp_max = parts[3].toDouble()
        val pressure = parts[4].toInt()
        val humidity = parts[5].toInt()
        val mai = Main(temp, feels_like, temp_min, temp_max, pressure, humidity)
        return mai
    }


    @TypeConverter
    @ToJson
    fun cloudsToString(value: Clouds): String {
        return value.all.toString()
    }
    @TypeConverter
    @FromJson
    fun stringToClouds(str: String): Clouds {
        val all = str.toInt()
        val clouds = Clouds(all)
        return clouds
    }

    @TypeConverter
    @ToJson
    fun windToString(value: Wind): String {
        return value.speed.toString() + ","+ value.deg.toString()
    }
    @TypeConverter
    @FromJson
    fun stringToWind(str: String): Wind {
        val delimiter = ","
        val parts = str.split(delimiter)
        val speed = parts[0].toDouble()
        val deg = parts[1].toInt()
        val wind = Wind(speed, deg)
        return wind
    }

}