package com.putevodka.myweatherapp.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Wind(
    @Json(name = "speed")
    val speed : Double,

    @Json(name = "deg")
    val deg : Int
):Serializable
