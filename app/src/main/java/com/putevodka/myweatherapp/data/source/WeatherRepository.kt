package com.putevodka.myweatherapp.data.source

import android.util.Log
import com.putevodka.myweatherapp.WeatherApplication
import com.putevodka.myweatherapp.data.source.remote.ApiInterface
import com.putevodka.myweatherapp.data.source.local.WeatherDao
import com.putevodka.myweatherapp.data.model.WeatherBase
import io.reactivex.Observable
import io.reactivex.Observable.concatArrayEager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WeatherRepository @Inject constructor(val apiInterface: ApiInterface,
                                            val weatherDao: WeatherDao
) {

    fun getWeather(): Observable<WeatherBase> {
        val observableFromApi = getWeatherFromApi()
        val observableFromDb = getWeatherFromDb()
        return concatArrayEager(observableFromApi, observableFromDb)
    }

    fun getWeatherFromApi(): Observable<WeatherBase> {
        return apiInterface.getWeatherRemote(WeatherApplication.latitude, WeatherApplication.longitude,
        WeatherApplication.API_KEY)
            .doOnNext {
                Log.d("REPOSITORY API * ", it.toString())
                    weatherDao.insertWeather(it)
            }
            .doOnError {
                Log.d("REPOSITORY API ERROR* ", it.toString())
            }
    }

    fun getWeatherFromDb(): Observable<WeatherBase> {
        return weatherDao.queryNewestWeather()
            .toObservable()
            .doOnNext {
                Log.e("REPOSITORY DB *** ", it.toString())
            }
            .doOnError {
                Log.d("REPOSITORY DB ERROR* ", it.toString())
            }
    }
}