package com.putevodka.myweatherapp.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.putevodka.myweatherapp.data.model.WeatherBase
import com.putevodka.myweatherapp.data.model.WeatherTypeAdapter

@Database (entities = arrayOf(WeatherBase::class), version = 1)
@TypeConverters(WeatherTypeAdapter::class)
abstract class Database : RoomDatabase(){
    abstract fun weatherDao() : WeatherDao
}