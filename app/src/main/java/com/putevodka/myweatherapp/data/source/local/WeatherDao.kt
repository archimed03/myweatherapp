package com.putevodka.myweatherapp.data.source.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.putevodka.myweatherapp.data.model.WeatherBase
import io.reactivex.Single

@Dao
interface WeatherDao {
    @Query("SELECT * FROM weathertable WHERE dt = (SELECT MAX(dt) FROM weathertable )")
    fun queryNewestWeather(): Single<WeatherBase>

    @Insert(
        onConflict = OnConflictStrategy.REPLACE
    )
    fun insertWeather(weather: WeatherBase)
}