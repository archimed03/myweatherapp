package com.putevodka.myweatherapp.data.source.remote

import com.putevodka.myweatherapp.data.model.WeatherBase
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("https://api.openweathermap.org/data/2.5/weather")
    fun getWeatherRemote(@Query("lat") lat:Double?, @Query("lon") lon:Double?,
                    @Query("appid") appid:String) : Observable<WeatherBase>

}