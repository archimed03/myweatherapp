package com.putevodka.myweatherapp.di.component

import android.app.Application
import com.putevodka.myweatherapp.WeatherApplication
import com.putevodka.myweatherapp.di.modules.AppModule
import com.putevodka.myweatherapp.di.modules.BuildersModule
import com.putevodka.myweatherapp.di.modules.NetModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(AndroidInjectionModule::class,
        BuildersModule::class, AppModule::class, NetModule::class)
)
interface AppComponent {
    fun inject(app: WeatherApplication)
}