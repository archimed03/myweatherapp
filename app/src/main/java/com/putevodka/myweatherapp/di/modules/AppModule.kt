package com.putevodka.myweatherapp.di.modules

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.putevodka.myweatherapp.data.source.local.Database
import com.putevodka.myweatherapp.data.source.local.WeatherDao
import com.putevodka.myweatherapp.ui.WeatherViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app:Application) {
    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideWeatherDatabase(app: Application): Database = Room.databaseBuilder(app,
        Database::class.java, "weathertable").build()

    @Provides
    @Singleton
    fun provideWeatherDao(database: Database): WeatherDao = database.weatherDao()

    @Provides
    fun provideWeatherViewModelFactory(
        factory: WeatherViewModelFactory): ViewModelProvider.Factory = factory

}