@file:Suppress("DEPRECATION")

package com.putevodka.myweatherapp.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.putevodka.myweatherapp.R
import com.putevodka.myweatherapp.WeatherApplication.Companion.latitude
import com.putevodka.myweatherapp.WeatherApplication.Companion.longitude
import com.putevodka.myweatherapp.data.model.WeatherBase
import com.putevodka.myweatherapp.databinding.ActivityMainBinding
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    private var isCancelled = true
    //Timer countdown
    // 60 seconds (1 minute)
    val minute:Long = 1000 * 60 // 1000 milliseconds = 1 second
    // 10 minutes 50 seconds
    val millisInFuture:Long =  (minute * 10) + (1000 * 50)
    // Count down interval 1 second
    val countDownInterval:Long = 1000


    @Inject
    lateinit var weatherViewModelFactory: WeatherViewModelFactory
    lateinit var weatherViewModel: WeatherViewModel
    private lateinit var binding: ActivityMainBinding

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.weather_toolbar))
        AndroidInjection.inject(this)

        weatherViewModel = ViewModelProviders.of(this, weatherViewModelFactory).get(
            WeatherViewModel::class.java)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        showWeather()
    }

    private fun showWeather() {
        var url:String
        obtieneLocalizacion()
        weatherViewModel.loadWeather()
        weatherViewModel.weatherResult().observe(this,
            Observer<WeatherBase> {
                binding.textView4.text = "city  ${it?.name}"
                val tempC = it?.main?.temp?.minus(273.15)?.toInt()
                binding.textView3.text = "${tempC.toString()}°C"
                binding.textView.text = "Wind  ${it.wind.speed} m/sec    ${it.wind.deg} degrees"
                binding.textView5.text = "${it.weather[0].description}"
                 url = "https://openweathermap.org/img/wn/${it.weather[0].icon}@2x.png"
                //url = "https://openweathermap.org/img/wn/13d@2x.png"
                loadImage(url)
            })

        weatherViewModel.weatherError().observe(this, Observer<String>{
            binding.textView4.text = "Hello error "
        })
    }

    private fun loadImage(weatherURL: String) {
        Glide.with(this)
            .load(weatherURL)
            .centerCrop()
            .error(ColorDrawable(Color.RED))
            .into(binding.imageView)
    }

    //@SuppressLint("MissingPermission")
    private fun obtieneLocalizacion(){

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                latitude =  location?.latitude
                longitude = location?.longitude
                Log.d("Obtain longitude !!!", longitude.toString())
            }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.weather_main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                val text = "Refresh button click!"
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(applicationContext, text, duration)
                toast.setGravity(Gravity.CENTER, 0, 0)
                toast.show()
                showWeather()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    // Method to configure and return an instance of CountDownTimer object
    private fun timer(millisInFuture:Long,countDownInterval:Long): CountDownTimer {
        return object: CountDownTimer(millisInFuture,countDownInterval){
            override fun onTick(millisUntilFinished: Long){
                val timeRemaining = timeString(millisUntilFinished)
                if (isCancelled){
                    binding.textView2.text = "${binding.textView2.text}\nStopped.(Cancelled)"
                    cancel()
                }else{
                    binding.textView2.text = timeRemaining
                }
            }
            override fun onFinish() {
                binding.textView2.text = "Done"
                val toast = Toast.makeText(applicationContext, "Request Weather!!!", Toast.LENGTH_SHORT)
                toast.show()
                //showWeather()
                isCancelled = true
            }
        }
    }

    // Method to get days hours minutes seconds from milliseconds
    private fun timeString(millisUntilFinished:Long):String{
        var millisUntilFinished:Long = millisUntilFinished
       // val days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished)
       // millisUntilFinished -= TimeUnit.DAYS.toMillis(days)

        //val hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished)
        //millisUntilFinished -= TimeUnit.HOURS.toMillis(hours)

        val minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
        millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes)

        val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)

        // Format the string
        return String.format(
            Locale.getDefault(),
            "%02d min: %02d sec",
            minutes,seconds
            //"%02d day: %02d hour: %02d min: %02d sec",
            //days,hours, minutes,seconds
        )
    }

    override fun onResume() {
        // Start the timer
        if(isCancelled)
        timer(millisInFuture,countDownInterval).start()
        isCancelled = false
        super.onResume()
    }

    override fun onDestroy() {
        weatherViewModel.disposeElements()
        isCancelled = true
        super.onDestroy()
    }

}