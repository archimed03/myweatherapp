package com.putevodka.myweatherapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.putevodka.myweatherapp.data.model.WeatherBase
import com.putevodka.myweatherapp.data.source.WeatherRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class WeatherViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository) : ViewModel()  {

    var weatherResult: MutableLiveData<WeatherBase> = MutableLiveData()
    var weatherError: MutableLiveData<String> = MutableLiveData()
    lateinit var disposableObserver: DisposableObserver<WeatherBase>

    fun weatherResult(): LiveData<WeatherBase> {
        return weatherResult
    }

    fun weatherError(): LiveData<String> {
        return weatherError
    }

    fun loadWeather() {
        disposableObserver = object : DisposableObserver<WeatherBase>() {
            override fun onComplete() {
            }

            override fun onNext(weather: WeatherBase) {
                weatherResult.postValue(weather)
            }

            override fun onError(e: Throwable) {
                weatherError.postValue(e.message)
            }
        }
        weatherRepository.getWeather()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe(disposableObserver)
    }

    fun disposeElements(){
        if(null != disposableObserver && !disposableObserver.isDisposed) disposableObserver.dispose()
    }
}